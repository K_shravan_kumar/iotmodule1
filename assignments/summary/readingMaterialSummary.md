# **Industrial Internet of Things**
<img src="https://miro.medium.com/max/700/1*92OdlxNqI3iChI5kNl1MFg.jpeg" height="400" width="600" >

## Industrial Revolution
<img src="https://www.onupkeep.com/answers/wp-content/uploads/2019/05/Industry4point0-768x512.png" height="700" width="900">

1. **Industry 1.0**: The First Industrial Revolution began in the 18th century through the use of steam power and mechanisation of production.
2. **Industry 2.0**: The Second Industrial Revolution began in the 19th century through the discovery of electricity and assembly line production. 
3. **Industry 3.0**: The Third Industrial Revolution began in the ’70s in the 20th century through partial automation using memory-programmable controls and computers.
4. **Industry 4.0**: This is characterised by the application of information and communication technologies to industry and is also known as "Industry 4.0". It builds on the developments of the Third Industrial Revolution.



![Image of mindmap](https://1.bp.blogspot.com/-g7jm2bwiFlg/Xyll-BtMmtI/AAAAAAAAIUg/-fSfn3TFHvMQqmswDn2gYlK3RaYblJYngCLcBGAsYHQ/s1600/readingMaterialSummary.JPG) 

## **Challenge for Industries as of now:** Make the shift form Industry 3.0 to 4.0  

## **Industry 3.0**

Data is stored in databases and represented in excels.

Let us dive into industry 3.0

**Field devices**

- sensors - A device which detects or measures a physical property and records, indicates, or otherwise responds to it.
- acutators - A device that causes a machine or other device to operate.

      
       

**Control Devices**
    
-  PCs
-  PLCs(Programmable Logic Controller)
-  CNCs(Computer Numerical Control)
- DCS(Distributed Control System)

**Stations** - Set of field devices and control devices form Stations.

**Work centers** - Set of Stations form work centers.(SCADA,HISTORIAN)

**Enterprise** - Set of work centers form Enterprise.(ERP,MES)

**Field Bus**

The bridge between the field devices and the control devices is called Field Bus.
![](https://image.slidesharecdn.com/ocg-jahresopening-2018-keynote-drobics-short-180126083643/95/iot-trends-in-industrial-iot-for-2018-26-638.jpg?cb=1516955982)


**3.0 Protocols**

- MODBUS
- PROFINET
- CANopen
- EtherCAT


All these protocols are optimized for sending data to a central server inside the factory.


## **Industry 4.0**

Industry 4.0 is Industry 3.0 devices connected to the Internet (IoT).

The reasons why industry 4.0 is important are the benefits. It helps manufacturers with current challenges by becoming more flexible and reacting to changes in the market easier. It can increase the speed of innovation and is very consumer centered, leading to faster design processes.

**Various operations by Indutry 4.0**

- Dashboard
- Remote Web SCADA
- Remote control configuration of devices
- Predictive Maintenance
- Real time event stream processing
- Analytics with predictive models
- Automated device provisioning, Auto Discovery
- Real time alerts and alarms

**Arhitecture**
![](https://openiotfog.org/images/OpenIoTFog2Architecture.png)

**4.0 Protocols**

- MQTT
- AMQP
- CoAP
- OPC UA
- Websockets
- HTTP
- REST API


**Problems in 4.0 Upgrades**

 1. Cost
 2. Downtime
 3. Reliability

**Solution**

Get data from Industry 3.0 devices/meters/sensors without changes to the original device. And then send the data to the Cloud using Industry 4.0 devices.

**But how?**

Convert Industry 3.0 protocols to Industry 4.0 protocols. These conversions can be done using libraries available to get data from Industry 3.0 devices and further send it to Industry 4.0 devices.

- Identify most popular Industry 3.0 devices.
- Study protocols that these devices communicate.
- Get data from the Industry 3.0 device.
- Send the data to cloud for Industry 4.0 device.

## **Tools**

### **IoT TSDB Tools** - Store data in Time series databases.

- InfluxDB
- Kdb+
- Prometheus
- Graphite
- RRDTool
- TimescaleDB
- OpenTSDB
- Druid
- FaunaDB
- Thingsboard

### **IoT Platforms** - To Analyze data

- AWS IOT
- Google IoT
- Azure IoT
- Thingsboard

### **Alerts** 

Get Alerts based on your data using these platforms.

- Zaiper
- Twilio  

## **Solution:**  
**Several libraries that act as Interfaces on embedded boards that have APIs doing the conversion from data from the PLC and sending it to cloud.  
IoT Gateways are hence much more economically friendly**  
![Raspberry Pi is an example](https://www.androidcentral.com/sites/androidcentral.com/files/styles/large/public/article_images/2017/02/raspberry-pi-3-3.jpg)

## **Appurtenances:**  
* 1. **Plenty of data collected and stored in Time series databases that convert it to beautiful diagrams where you can visualize the data with ease**  
![IoT Dashboard](https://blog.thethings.io/wp-content/uploads/2016/02/Screen-Shot-2017-01-23-at-5.08.33-PM-9.png)
* 2. **IoT Platforms that analyse data such as Azure and AWS, they make heavy server systems and other related hardware obsolete**  
![Available Platforms](https://4zy7s42hws72i51dv3513vnm-wpengine.netdna-ssl.com/wp-content/uploads/2018/02/cloud-platform-comparative-listings-img.jpg)
* 3. **Alerts can be sent to our devices through GPS  with platforms like Twilio**  
![How alert system works](https://images.ctfassets.net/2fcg2lkzxw1t/6E47U6Ach2AMMq8ks40Q8i/0b5225cd3f83789f4fa0923878adf119/bulk-sms-mass-text-message-notification-marketing-twilio-api-2.png)
[**documentation by K_Shravan Kumar**](https://gitlab.com/K_shravan_kumar)
